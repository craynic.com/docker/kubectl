FROM docker:28.0@sha256:9a651b22672c7151b5d8ca820ed2290b3fe4d4922e9b3f37ab14c8876da6613d

# renovate: datasource=repology depName=alpine_3_21/kubectl depType=dependencies versioning=loose
ARG KUBECTL_VERSION="1.31.5-r0"

RUN apk --no-cache add \
        kubectl="${KUBECTL_VERSION}" \
    && apk --no-cache upgrade libretls \
    && apk upgrade --no-cache \
        # various CVEs fixed in 3.0.8
        libssl3 libcrypto3 \
        # fixed in 1.36.1-r2
        busybox busybox-binsh ssl_client
